## Frekvenční rovnice
```math
\lambda =C_{0} T
\,,\;
T =\frac{1}{f}
\,,\;
2 \pi f =\Omega
\,,\;
\frac{\Omega}{C_{0}} =\alpha
\;\Rightarrow\;
\lambda=\frac{2 \pi}{\alpha}
```
## Vetknutý-Vetknutý
```math
\left.\begin{array}{l}
\begin{aligned}
	\frac{1}{2} \lambda_{1}&=L \\
	\lambda_{2}&=L \\
	\frac{3}{2} \lambda_{3}&=L
\end{aligned}
\end{array}\right\}
	\frac{n}{2} \lambda_{n}=L
	\;\Rightarrow\;
	\alpha=\pi \frac{n}{L}
```
<!-- ![Vetknutý-Vetknutý](VetknutyVetknuty.png) -->
<img src="VetknutyVetknuty.png" width="400" >

# Volný-Volný
```math
\left.\begin{array}{l}
\begin{aligned}
	\frac{1}{2} \lambda_{1}&=L \\
	\lambda_{2}&=L \\
	\frac{3}{2} \lambda_{3}&=L
\end{aligned}
\end{array}\right\}
	\frac{n}{2} \lambda_{n}=L
	\;\Rightarrow\;
	\alpha=\pi \frac{n}{L}
```
<!-- ![Volný-Volný](VolnyVolny.png) -->
<img src="VolnyVolny.png" width="400" >

## Vetknutý-Volný
```math
\left.\begin{array}{l}
\begin{aligned}
	\frac{1}{4} \lambda_{1}&=L \\
	\frac{3}{4} \lambda_{2}&=L \\
	\frac{5}{4} \lambda_{3}&=L
\end{aligned}
\end{array}\right\}
	\frac{2n-1}{4} \lambda_{n}=L
	\;\Rightarrow\;
	\alpha=\pi \frac{2n-1}{L}
```

<!-- ![Vetknutý-Volný](VetknutyVolny.png) -->
<img src="VetknutyVolny.png" width="400" >