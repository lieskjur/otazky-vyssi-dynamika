# Otázky z vyšší dynamiky
* [02 Vazby](02_Vazby.md)
* [03 D'Alembertův princip](03_DAlembert.md)
* [04 Lagrangeovy rovnice](04_LagrangeovyRovnice.md)
* [07-08 Variační počet](07-08_VariacniPocet.md)
* [09 Vázané variační úlohy](09_VazaneVariacniUlohy.md)
* [10 Integrální a Hamiltonův princip](10_IntegralniHamiltonuvPrincip.md)
* [12-14 Podélné kmitání](12-16_PodelneKmity.md)
* [15 Kmitání vzduchového sloupce](15_KmitaniVzduchovehoSloupce.md)
* [? Frekvenční rovnice](FrekvencniRovnice.md)
* [17-18 Ohybové kmity](17_OhyboveKmity.md)
* [Poddajná Tělesa](PoddajnaTelesa.md)

<!-- for f in *.md; cat $f; echo; echo; end > VD.md -->