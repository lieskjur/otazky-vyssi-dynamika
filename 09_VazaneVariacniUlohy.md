# 09 Vázané variační úlohy
Funkcionál
```math
I(y(x)) = \int_a^b F\left(x,y(x),y^\prime(x)\right) dx
```
s vedlejší podmínkou ve tvaru
* integrálním: $`\int_a^b G(x,y,y^\prime) dx = \text{konst}`$
* algebraickém $`g(x,y) = 0`$
* diferenciálním $`g(x,y,y^\prime) = 0`$

Variační úlohu řeším jako úlohu s funkcí
```math
F^* = F\left(x,y(x),y^\prime(x)\right) + λ⋅g(x,y)
```