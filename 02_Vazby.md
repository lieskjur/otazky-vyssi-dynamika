# 02 Vazby
## Podle závislosti na čase
- skleronomní (stacionární)
- neonomní (nestacionární)
## Podle závislosti na souřadnicích
- holonomní: pouze poloha
- neholonomní: vyšší derivace

Je-li možné integrací převést vazbu na holonomní, nazýváme ji *semi-holonomní*