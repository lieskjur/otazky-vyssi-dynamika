# 10 Integrální a Hamiltonův princip
## Hamiltonův princip
Zavedeme-li *zobecněnou hybnost*
```math
	p=\frac{\partial L}{\partial \dot{q}}
```
lze na základě Eulerově diferenciální rovnice
```math
\frac{\partial L}{\partial y}-\frac{d}{d x}\left(\frac{\partial L}{\partial y^{\prime}}\right)=0
```
kde je variován Lagrangián $`L = E_k - E_p`$ odvodit
```math
\left.\begin{array}{r}
	p=\frac{\partial L}{\partial \dot{q}} \\
	\frac{d}{d t}\left(\frac{\partial L}{\partial  \dot{q}}\right)-\frac{\partial L}{\partial q}=0
\end{array}\right\}
\Rightarrow
\dot{p}=\frac{\partial L}{\partial q}
```
a následně
```math
\left.\begin{array}{r}
	\delta L= \frac{\partial L}{\partial q} \cdot \delta q+\frac{\partial L}{\partial \dot{q}} \cdot \delta \dot{q} \\
	\frac{\partial L}{\partial q} = \dot{p} \\
	\delta(p \cdot \dot{q})=\delta p \cdot \dot{q}+p \cdot \delta\dot{q}
\end{array}\right\}
\Rightarrow
\delta( L-p\cdot\dot{q}) = \dot{p}\cdotδq - δp\cdot\dot{q}
```
### Hamiltonovy kanonické rovnice
Pro *Hamiltonovu funkci*
```math
	H= p\cdot\dot{q}-L
```
lze vyjádřit pohybové rovnice
```math
\begin{aligned}
	\dot{q}&=\frac{\partial H}{\partial p} \\
	\dot{p}&=\frac{\partial H}{\partial q}
\end{aligned}
```
Těch je sice 2 krát více, než Lagrangeových rovnic, ale jsou prvního řádu a projevují stabilnější chování při numerických výpočtech.

## Integrální princip
Zkoumá soustavu v daném časovém intervalu (tzv. globální chování soustavy
<img src="IntegralniPrincip.png" width="200">
<!-- ![Integralní princip](IntegralniPrincip.png "Integralní princip") -->
