# 04 Lagrangeovy rovnice
## I. druhu
Vycházejí přímo z rozšířeného D'Alembertova principu pro holonomní vazby
```math
\begin{aligned}
F_{i}-m_{i} a_{i}+\sum \lambda_{j} \frac{\partial f_{j}}{\partial r_{i}}=0 \\
\left(M_{i}-\left(I_{i} \omega_{i}+\omega_{i} \times I_{i} \omega_{i}\right)\right) B+\sum \lambda_{i} \frac{\partial f_{j}}{\partial \gamma_{i}}=0
\end{aligned}
```
## II. druhu
pro pohybové rovnice v nezávislých souřadnicích (bez vazeb)
```math
\frac{d}{d t}\left(\frac{\partial E_{k}}{\partial q_{j}}\right)-\frac{\partial E_{k}}{\partial q_{j}}=Q_{j}
\;,\quad 
j=1,\ldots,n
```
* $`n`$ - počet stupňů volnosti
* $`q_j`$ - nezávislé (zobecněné) souřadnice
* $`E_k`$ - kinetická energie
* $`E_p`$ - potenciální energie
* $`Q_j`$ - zobecněné vnější síly

## Smíšeného typu
```math
\frac{d}{d t}\left(\frac{\partial E_{k}}{\partial \dot{s}_{j}}\right)-\frac{\partial E_{k}}{\partial s_{j}}=S_{j}+\sum_{k=1}^{r} \lambda_{k} \frac{\partial f_{k}}{\partial s_{j}^{(n)}}-\frac{\partial E_{p}}{\partial s_{j}}-\frac{\partial D}{\partial \dot{s}_{j}}
\;,\quad 
j=1,\ldots,Nm
\,,\;
r = Nm-n
```
* $`N`$ - počet stupňů volnosti uvolněného tělesa
* $`m`$ - počet těles
* $`n`$ - počet stupňů volnosti mechanismu
* $`s_j`$ - fyzikální souřadnice
* $`E_k`$ - kinetická energie
* $`E_p`$ - potenciální energie
* $`S_j`$ - vnější silové účinky
* $`f_k(s,\dot{s},\ldots,\overset{n}{s},t) = 0`$ - vazbové rovnice
* $`\lambda_k`$ - Lagrangeovy multiplikátory
* $`D = \frac{1}{2} \sum_i \sum_j b_{ij} \dot{s}_i \dot{s}_j`$ - Rayleigho disipativní funkce