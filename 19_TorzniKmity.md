# 19 Torzní kmity
## Pohybová rovnice
```math
I \frac{\partial^{2} \varphi}{\partial t^{2}}=M(x)+\frac{\partial M}{\partial x} d x-M(x)
```
```math
M=G J_{P} \frac{\partial \varphi}{\partial x}
\;,\quad 
I=\rho J_{P} d x
```
```math
\begin{aligned}
\rho J_{P} d x \frac{\partial \varphi}{\partial t^{2}} & =\frac{\partial}{\partial x}\left(G J_{P} \frac{\partial \varphi}{\partial x}\right) d x & \\
\frac{\partial^{2} \varphi}{\partial t^{2}} & =C_{0}^{2} \frac{\partial^{2} \varphi}{\partial x^{2}}
\;,\quad 
C_{0}=\sqrt{\frac{G}{\rho}}
\end{aligned}
```
## Metoda oddělených proměnných
Předpokládáme řešení ve tvaru
```math
\varphi=\varphi(x, t)=X(x) T(t)
```
Kdy po dosazení můžeme pohybovou rovnici upravit do tvaru
<!-- ```math
\begin{aligned}
X(x) \dot{T}(t) &= C_{0}^{2} X^{\prime \prime}(x) T(t)\\
\frac{\ddot{T}(t)}{T(t)} &= C^{2} \frac{X^{\prime \prime}(x)}{X(x)}
\end{aligned}
``` -->
```math
\frac{\ddot{T}(t)}{T(t)} = C_0^{2} \frac{X^{\prime \prime}(x)}{X(x)}
```
Aby rovnice platila musí se obě strany rovnat konstantě, za kterou zvolíme $`-\Omega^2`$. Následně můžeme hledat řešení obou stran nezávisle
```math
\begin{aligned}
\ddot{T}+\Omega^{2} T &= 0 &\Rightarrow T(t) &= C \sin (\Omega t+\varphi) \\
C_{0}^{2} X^{\prime \prime}+\Omega^{2} X &= 0 &\Rightarrow X(t) &= A \sin (\alpha x)+B \cos (\alpha x)
\;,\quad
\alpha^{2}=\frac{\Omega^{2}}{C_{0}^{2}}
\end{aligned}
```
řešení bude nabývat tvaru
```math
\varphi(x,t)=\sum_{n=1}^{\infty}\left(A_{n} \sin \left(\sqrt{\frac{\Omega_{n}^{2}}{C_{0}^{2}}} x\right)+B \cos \left(\sqrt{\frac{\Omega_{n}^{2}}{C_{0}^{2}} x}\right)\right) C_{n} \sin \left(\Omega_{n}+\varphi_{n}\right)
```
kde $`C_n`$, $`\varphi`$ budeme určovat z počátečních a $`A_n`$, $`B_n`$ z okrajových podmínek