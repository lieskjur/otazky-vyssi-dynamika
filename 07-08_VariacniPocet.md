# 07-08 Variační počet
## Podmínky lokálního minima
```math
I(x) = \int_a^b F\left(x\right) dx
```
Eulerova podmínka lokálního extrému
```math
δI\big(x,v\big) = 0
```
Lagrangeova nutná podmínka lok. minima
```math
δ^2I\big(x,v,v\big) ≥ 0
```

## Funkcionály různých typů
### Jedna nezávislá proměnná, derivace prvního řádu
```math
I(y(x)) = \int_a^b F\left(x,y(x),y^\prime(x)\right) dx
```
*Eulerovu podmínku* lze zapsat jako rovnici
```math
δI\big(y(x)\big)
=
\int_{a}^{b}\left(\frac{\partial F}{\partial y}-\frac{d}{d x}\left(\frac{\partial F}{\partial y^{\prime}}\right)\right) \delta y \, d x=0
```
z které vychází *Eulerova dif. rovnice*
```math
\frac{\partial F}{\partial y}-\frac{d}{d x}\left(\frac{\partial F}{\partial y^{\prime}}\right)=0
```
kterou lze dál rozepsat na
```math
F_{y}-F_{x y^{\prime}}-F_{y y^{\prime}} y^{\prime}-F_{y^{\prime} y^{\prime}} y^{\prime \prime}=0
```
<!-- ```math
\frac{\partial F}{\partial y}
-\frac{\partial^2 F}{\partial x y^{\prime}}
-\frac{\partial^2 F}{\partial y y^{\prime}} y^{\prime}
-\frac{\partial^2 F}{\partial y^{\prime} y^{\prime}} y^{′′}
=0
``` -->
Zatímco z *Lagrangeovy nutné podmínky* vyplývá *Lagendrova nutná podmínka*
```math
F_{x^{\prime} x^{\prime}} ≥ 0
```
<!-- \frac{∂^2 F}{∂ x^{\prime} x^{\prime}} ≥ 0 -->
### Jedna nezávislá proměnná, derivace vyšších řádů
```math
I(y(x)) = \int_a^b F\big(x,y(x),y^\prime(x),\ldots,y^{(n)}(x)\big) dx
```
Podmínka extrému (Euler Poissonova dif. rovnice)
```math
\frac{\partial F}{\partial y}-\frac{d}{d x}\left(\frac{\partial F}{\partial y^{\prime}}\right)+\frac{d^{2}}{d x^{2}}\left(\frac{\partial F}{\partial y^{\prime \prime}}\right)+\ldots+(-1)^{n} \frac{d^{n}}{d x^{n}}\left(\frac{\partial F}{\partial y^{(n)}}\right)=0
```
### dvě nezávislé proměnné, derivace prvního řádu
```math
I(u(x, y))=\int_{a}^{b} \int_{c}^{d} F\left(x, y, u, \frac{\partial u}{\partial x}, \frac{\partial u}{\partial y}\right) d x d y
```
Podmínka extrému (Euler-Ostrogradskéo rovnice)
```math
\frac{\partial F}{\partial u}-\frac{\partial}{\partial x}\left(\frac{\partial F}{\partial u_{x}}\right)-\frac{\partial}{\partial x}\left(\frac{\partial F}{\partial u_{y}}\right)=0 \;;\quad u_{x}=\frac{\partial u}{\partial x} \,,\; u_{y}=\frac{\partial u}{\partial y}
```