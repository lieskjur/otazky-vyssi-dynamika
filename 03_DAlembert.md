# 03 D'Alembertův princip
```math
\delta W=\sum_{i=1}^{N} (\vec{F}_{i}-m_i\vec{a}_i) \cdot \delta \vec{r}_{i}=0
```
kde $`\delta W`$ je virtuální práce (virutální přírůstek energie), $`\delta \vec{r}_{i}`$ jsou virtuální přírůstky polohy, $`\vec{F}_{i}`$ zobecněná síla a člen $`m_i\vec{a}_i`$ D'Alembertovy setrvačné síly.

## Rozšíření pro holonomní vazby
```math
\delta W=\sum_{i=1}^{N}\left(\vec{F}_{i}-m_i\vec{a}_i+\sum_{j=1}^r \lambda_{j} \frac{\partial f_j}{\partial \vec{r}_{i}}\right) \cdot \delta \vec{r}_{i}=0
```
kde $`f_j(\vec{r}_1,\ldots,\vec{r}_N,t)`$ jsou holonomní vazbové rovnice a $`\lambda_j`$ Lagrangeovy multiplikátory

---
**poznámka**: pod virtuálními přírůstky myslíme myšlené možné ve shodě s vazbami a teké předpokládáme že jsou *isochronní* (tzn. nezávislé na čase)