# Poddajná tělesa
## Radius vektor k bodu Tělesa
```math
r_{p}(\xi, q)=R+S(\Phi)(x(\xi)+u(\xi, q_f))
```
<!-- ![Poddajné těleso](PoddajneTeleso.png) -->
<img src="PoddajneTeleso.png" width="400">

## 

## Vnitřní deformační energie
```math
U=\frac{1}{2} \int \varepsilon^T \sigma d V \\
```
```math
\sigma=C \varepsilon
\;,\quad 
\varepsilon=D u
\;,\quad 
u=A(\xi) q_{f} \\
```
```math
U=\frac{1}{2} q_{f}^T \underbrace{ \int_{V_{0}} A^T D^T C D A d \xi}_{K} q_{f}
```
### Zobecněná vnitřní síla
```math
\delta q_{F}^T Q_{I}=\delta U=\frac{\partial U}{\partial q_{f}} \delta q_{f}=\delta q_{f}^T K q_{f} \Rightarrow Q_{I}=K q_{f}
```
## Působení vnějších sil
```math
\delta W=\delta q^T Q_{E}=\delta \vec{r}_{P} \cdot \vec{F}
```
kde $`\delta W`$ je virtuální práce, $`Q_{E}`$ zobecněná vnější síla a $`\delta r_P`$ virtuální změna radius vektoru k bodu $`P`$
```math
\delta r_{P} =\delta R+\delta S(x+u)+S \delta u
=
\underbrace{
	\begin{bmatrix}
		E & B & SA
	\end{bmatrix}
}_{L} \delta q	
```
kde
```math
B=\frac{\partial S}{\partial \Phi}(x+u)
\;,\quad 
A=\frac{\partial u}{\partial q_{f}}
```
### Zobecněná vnější síla
```math
Q_E = L^T \vec{F}
```