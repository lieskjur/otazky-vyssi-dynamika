# 15 Kmitání vzduchového sloupce
## Pohybová rovnice
```math
A\left(\rho+\rho_{a}\right) d x\left(\frac{\partial v}{\partial t}+v \frac{\partial v}{\partial x}\right)=A p-A\left(p+\frac{\partial p}{\partial x} d x\right)
```
lze upravit do tvaru
```math
\frac{\partial v}{\partial t}+v \frac{\partial v}{\partial x}=-\frac{1}{\rho+\rho_a} \frac{\partial p}{\partial x}
```
## Rovnice kontinuity
Rovnici kontinuity
```math
\frac{d}{d t}(m)=\frac{d}{d t}(\rho A d x)=0
```
pro $`A=\text { konst }`$ lze s mezikrokem
```math
\frac{d \rho}{d t} d x+d\left(\rho+\frac{d x}{d t}\right)=0
```
upravit do tvaru
```math
\frac{d \rho}{d t}+\frac{d \rho}{d x}+\rho \frac{d^{2} x}{d t^2}=0
```
## Stavová rovnice
ze stavové rovnice
```math
p v^{\kappa}=p \rho^{-\kappa}=\text { konst }
```
lze odvodit
```math
\frac{\partial p_{a}}{\partial t}=C_{0}^{2} \frac{\partial \rho_{a}}{\partial t}, \quad C_{0}^{2}=\frac{p_{o} \kappa}{\rho_{o}}
```
## Výsledná pohybová rovnice
Po dosazení a úpravě získáme
```math
\frac{\partial^{2} p}{\partial x^{2}}=\frac{1}{c_{0}^{2}} \frac{\partial^{2} p}{\partial t^{2}}
```