# 21 Přibližná Řešení
## metoda rozvoje v nekonečnou řadu
```math
\begin{aligned}
q&=\frac{4 g}{\pi} \sum_{i=1,3,5, \cdots}^{\infty} \frac{1}{i} \sin \left(i \pi \frac{x}{L}\right) \\
\bar{y}&=\sum_{i=1,3,5, \ldots}^{\infty} a_{i} \sin \left(i \pi \frac{x}{L}\right)
\end{aligned}
```
```math
\begin{aligned}
E J y^{i v} &=q \\
E J \sum_{i=1,3,5, \ldots}^{\infty}\left(\frac{i \pi}{L}\right)^{4} a_{i} \sin \left(i \pi \frac{x}{L}\right) &=\frac{4 g}{\pi} \sum_{i=1,3,5, \ldots}^{\infty} \frac{1}{i} \sin \left(i \pi \frac{x}{L}\right) \\
a_{i} &=\frac{4 g L^{4}}{E J \pi^{4} i^5}
\;,\quad 
i=1,3,5
\end{aligned}
```
## Kolokační metoda
```math
\bar{y}(x) = \sum a_i \varphi_i(x) \;,\quad \bar{y}(x_i) \overset{!}{=} y(x_i)
```
kde $`\varphi_i(x)`$ jsou tvarové funkce (splňují okrajové podmínky) a $`x_i`$ jsou body kolokace.

## Galerkinova metoda
```math
\bar{y}(x) = \sum a_i \varphi_i(x)
```
```math
EJ \bar{y}^{IV}(x) \approx q(x)
```
### Funkce residuí
```math
\Delta\psi(x) = EJ \sum a_i \varphi_i^{IV}(x) - q(x)
```
### podmínka ortonormality
```math
\int_{0}^{l} \Delta \psi \cdot \varphi_{i}(x) d x \stackrel{!}{=} 0
\;,\quad 
i=1, \ldots, n
```
"zbytkové zatížení $`\Delta\psi`$ nekonají prác při virtuálním vychýlení nosníku z ktréhokoliv průhbyu daného tvarovými funkcemi $`\varphi_i(x)`$"

## Ritzova metoda (energetická)
* zvolím tvarové funkce
* sestavím funkcionál (obecně $`\Pi = \Pi(a_i)`$)
* z podmínky minima $`\frac{\partial \Pi}{\partial a_i} = 0`$ určím koef $`a_i`$
```math
\pi=U-W=\int_{0}^{l} \frac{1}{2} E J y^{\prime\prime^2}(x) d x-\int_{0}^{l} g y(x) d x
```