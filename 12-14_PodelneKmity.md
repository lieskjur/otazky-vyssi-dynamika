# 12-14 Podélné kmitání
## Pohybová rovnice
```math
\underbrace{\rho A d x}_{m} \frac{\partial^{2} \xi}{\partial t^2}=F(\xi+d x)-F(\xi)
```
```math
F(\xi+d x) \doteq F(\xi)+\frac{\partial F}{\partial x}(\xi) d x
```

```math
F=A \sigma=A E \varepsilon=A E \frac{\partial \xi}{\partial x}
```

```math
\begin{aligned}
\rho A d x \frac{\partial^{2} \xi}{\partial t^{2}}&= \frac{\partial}{\partial x}\left(E A \frac{\partial \xi}{\partial x}\right) d x \\
\frac{\partial^{2} \xi}{\partial t^{2}}&=C_{0}^{2} \frac{\partial \xi}{\partial x^{2}} \quad, \quad C_{0}=\sqrt{\frac{E}{\rho}}
\end{aligned}
```

## Metoda oddělených proměnných
Předpokládáme řešení ve tvaru
```math
\xi=\xi(x, t)=X(x) T(t)
```
Kdy po dosazení můžeme pohybovou rovnici upravit do tvaru
<!-- ```math
\begin{aligned}
X(x) \dot{T}(t) &= C_{0}^{2} X^{\prime \prime}(x) T(t)\\
\frac{\ddot{T}(t)}{T(t)} &= C^{2} \frac{X^{\prime \prime}(x)}{X(x)}
\end{aligned}
``` -->
```math
\frac{\ddot{T}(t)}{T(t)} = C_0^{2} \frac{X^{\prime \prime}(x)}{X(x)}
```
Aby rovnice platila musí se obě strany rovnat konstantě, za kterou zvolíme $`-\Omega^2`$. Následně můžeme hledat řešení obou stran nezávisle
```math
\begin{aligned}
\ddot{T}+\Omega^{2} T &= 0 &\Rightarrow T(t) &= C \sin (\Omega t+\varphi) \\
C_{0}^{2} X^{\prime \prime}+\Omega^{2} X &= 0 &\Rightarrow X(t) &= A \sin (\alpha x)+B \cos (\alpha x)
\;,\quad
\alpha^{2}=\frac{\Omega^{2}}{C_{0}^{2}}
\end{aligned}
```
řešení bude nabývat tvaru
```math
\xi(x,t)=\sum_{n=1}^{\infty}\left(A_{n} \sin \left(\sqrt{\frac{\Omega_{n}^{2}}{C_{0}^{2}}} x\right)+B \cos \left(\sqrt{\frac{\Omega_{n}^{2}}{C_{0}^{2}} x}\right)\right) C_{n} \sin \left(\Omega_{n}+\varphi_{n}\right)
```
kde $`C_n`$, $`\varphi`$ budeme určovat z počátečních a $`A_n`$, $`B_n`$ z okrajových podmínek


| Uložení			| 		| $`x=0`$							| $`x=l`$							|
| ---				| ---	|	--- 							| --- 								|
| vetknutý konec	|		| $`\xi(0,t) = 0`$ 					| $`\xi(0,t) = 0`$ 					|
| volný konec		|		| $`F(0)=0`$ 						| $`F(l)=0`$ 						|
| pružina			|		| $`F(0)=k\xi`$ 					| $`F(l)=k\xi`$ 					|
| tlumič			|		| $`F(0)=b\frac{∂\xi}{∂t}`$ 		| $`F(l)=-b\frac{∂\xi}{∂t}`$ 		|
| hmota				|		| $`F(0)=m\frac{∂^2\xi}{∂t^2}`$ 	| $`F(l)=-m\frac{∂^2\xi}{∂t^2}`$ 	|

kde $`F(x) = EA \frac{∂\xi}{∂x}(x)`$