# 17-18 Ohybové kmity
## Pohybovová rovnice
Základní pohybovou rovnici
```math
\rho A d x \frac{\partial y^{2}}{\partial t^{2}} =T+\frac{\partial T}{\partial x} d x-T
```
upravíme do tvaru
```math
\frac{\partial y^{2}}{\partial t^{2}} =\frac{1}{\rho A} \frac{\partial T}{\partial x}
```
Ze Schwedlerovy věty a approximace délky oblouku víme
```math
-q=\frac{d T}{d x}
\;,\quad 
T=\frac{d M_{0}}{d x}
\;,\quad 
\frac{M_{0}}{EJ} \approx y^{\prime \prime}
```
Po dosazení získáme
```math
\ddot{y}+C_{0}^{2} y^{IV}=0 \quad, C_{0}^{2}=\frac{E J}{\rho A}
```

## Řešení
Předpokládáme řešení ve tvaru
```math
y(x, t)=X(x) T(t)
```
Kdy po dosazení můžeme pohybovou rovnici upravit do tvaru
```math
-\frac{T(t)}{T(t)}=C_{0}^{2} \frac{X^{I V}(x)}{x(x)}
```
Aby rovnice platila musí se obě strany rovnat konstantě, za kterou zvolíme $`\Omega^2`$. Následně můžeme hledat řešení obou stran nezávisle
```math
\begin{aligned}
T+\Omega^{2} T&=0 &\Rightarrow \alpha^{2}&=\Omega^{2} \\
X^{IV}-\frac{\Omega^{2}}{C_{0}^{2}} X&=0 &\Rightarrow \alpha^{4}&=\frac{\Omega^{2}}{C_{0}^{2}}=\frac{\rho A}{E J} \Omega^{2}
\end{aligned}
```
```math
\begin{aligned}
T(t) &= A \cos(\Omega t) + B \sin(\Omega t) \\
X(x) &= C_1 \cosh(\alpha x) + C_2 \sinh(\alpha x) + C_3 \cosh(\alpha x) + C_4 \sinh(\alpha x)
\end{aligned}

```

## Okrajové podmínky
![Okrajové podmínky](Ohyb-OkrajovePodminky.png)